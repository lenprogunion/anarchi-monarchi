use yew_router::prelude::*;
use yew::prelude::*;

use crate::components::pages;


#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/account")]
    Account,
    #[at("/login")]
    Login,
    #[at("/compositions")]
    Compositions,
    #[at("/friends")]
    Friends,
    #[at("/chats/:chat_id")]
    Chats {chat_id: String},
    #[at("/register")]
    Register,
    #[at("/secure")]
    Secure,
    #[at("/lesson/:lesson_id")]
    Lesson {lesson_id: String},
    #[not_found]
    #[at("/404")]
    NotFound,
}

pub fn switch(routes: Route) -> Html {
    
    match routes {
        Route::Home => html!{<pages::home::Home/>},
        Route::Account => html!{<pages::account::Account/>},
        Route::Compositions => html!{<pages::compositions::Compositions/>},
        Route::Chats {chat_id} => html!{ <pages::chats::Chats {chat_id}/>},
        Route::Friends =>html!{  <pages::friends::Friends/>},
        Route::Register => html!{ <pages::register::Register/>},
        Route::Lesson {lesson_id} => html!{<pages::lesson::Lesson {lesson_id}/>},
        Route::Login => html!{<pages::login::Login />},
        Route::Secure => html! {
           
        },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}