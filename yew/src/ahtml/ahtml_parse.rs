use yew::{prelude::html, Html};

pub fn parser(text: String) -> Vec<Html> {                                         
    let tokens = lexer(text.clone());
    parse_to_html(tokens.clone())
}
#[derive(Clone, Debug)]
enum Token{
    Tag{
        name: String,
        value: String,
    },
}

fn parse_to_html(tokens: Vec<Token>) -> Vec<Html>{
    let mut tokens = tokens.iter();
    let mut result = Vec::<Html>::new();
    
    while let Some(token) = tokens.next(){
        match token{
            Token::Tag { name, value } => {
                match name.as_str(){
                    "Title" => {
                        result.push(html!{<div class="main__title">{value.to_string()}</div>});
                    },
                    "Text" => {
                        result.push(html!{<div class="main__text">{value.to_string()}</div>});
                    },
                    "Subtext" => {
                        result.push(html!{<div class="main__subtext">{value.to_string()}</div>})
                    },
                    "Image" => {
                        result.push(html!{<img src={value.to_string()}/>});
                    },
                    _ =>{},
                }
            },
            _ => {
                
            }
        }
    }
    result
}

fn lexer(text: String) -> Vec<Token>{
    let mut result = Vec::<Token>::new();
    let mut word = String::new();
    let mut tag_name = String::new();
    let mut text = text.chars();
    
    while let Some(symbol) = text.next(){
        match symbol{
            ':' =>{
                if !tag_name.is_empty(){
                    word.push(symbol);
                    continue;
                }
                tag_name = String::from(word.clone().trim());
                word.clear();
            },
            '"' => {
                if word.is_empty() || word == " "{
                    word.clear();
                    continue;
                }
                result.push(Token::Tag { name: tag_name.clone(), value: word.clone() });
                word.clear();
                tag_name.clear();
            },
            _ => {
                word.push(symbol);
            }
        }
    }
    result
}