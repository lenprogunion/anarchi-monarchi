use yew::prelude::*;
use yew_router::{
    BrowserRouter,
    hooks,
    history,
    Switch, 
    prelude::*,
};

mod components;
mod route;
mod ahtml;
use crate::route::{
    Route,
    switch,
};
pub use crate::components::{
    elements,
    pages,
    permanent::{
        header::Header,
        footer::Footer,
    },
};

#[derive(Clone, PartialEq, serde::Deserialize, Default)]
pub struct FullUserInfo {
    id: i32,
    username: String,
    email: String,
    password: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
}
#[function_component]
fn App() -> Html {
    html! {
        <BrowserRouter>
            <div class="container">
                <Header/>
                <Switch<Route> render={switch} />
                <Footer/>
            </div>
        </BrowserRouter>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}