use yew::prelude::*;
use yew_router::prelude::use_location;

#[function_component]
pub fn Footer() -> Html {
    let location = use_location().unwrap();
    let add_class_location = location.path() == "/account";
    let chats_location = location.path().contains("/chats");
    if chats_location{
        return html!{
            <footer></footer>
        };
    }
    html!{
        <footer class={classes!("footer", if add_class_location {"footer__full"} else {""})}>
            <div class={classes!("footer__copyleft")}>{"Copyleft"}<a class={classes!("footer__link")}>{"lenprogunion"}</a></div>
        </footer>
    }
}