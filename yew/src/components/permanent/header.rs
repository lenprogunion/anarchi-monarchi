use yew::prelude::*;
use yew_router::prelude::*;

use crate::Route;
use crate::elements::svg::{Svg, SvgIcons};

#[function_component]
pub fn Header() -> Html {
    let location = use_location().unwrap();
    let add_button_back = location.path() == "/account";
    let chats_back = location.path().contains("/chats");
    let navigator = use_navigator().unwrap();
    let onclick = Callback::from(move |_| navigator.back());
    let navigator = use_navigator().unwrap();
    let onclick_home = Callback::from(move |_| navigator.push(&Route::Home));
    html!{
        <header class={classes!("header")}>
            if add_button_back{
                <button  class={classes!("header__button")} {onclick}><Svg name={SvgIcons::BackArrow}/></button>
            }
            if chats_back{
                <button  class={classes!("header__button")} onclick={onclick_home}><Svg name={SvgIcons::BackArrow}/></button>
            }
            <Link<Route> classes={classes!("header__logo")} to={Route::Home}>{"Anarcho Monarcho"}</Link<Route>>
        </header>
    }
}