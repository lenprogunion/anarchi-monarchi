use yew::prelude::*;
use yew_router::prelude::Link;
use web_sys::window;
use crate::route::Route;
use crate::elements::svg::{Svg, SvgIcons};

#[derive(Clone, serde::Deserialize, PartialEq)]
pub struct Dialogs {
    id: i32,
    username: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
    dialog_id: i32,
}


#[function_component]
pub fn Aside() -> Html {
    let user_id = window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap().unwrap();
    let dialogs_handle = use_state(|| Vec::<Dialogs>::new());
    let dialogs = (*dialogs_handle).clone();

    use_effect_with_deps(move |_| {
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get(&format!("http://localhost:8086/api/show_dialog/{}", user_id))
                .await.unwrap()
                .json::<Vec<Dialogs>>().await.unwrap();
            match Some(res){
                Some(d) => {
                    dialogs_handle.set(d);
                },
                None =>{

                }
            }
        });
    },
    ());
    let mut dialogs_vnode = Vec::<Html>::new();
    for dialog in &dialogs{

        dialogs_vnode.push(
            html!{
                <Link<Route> to={Route::Chats{chat_id: dialog.dialog_id.clone().to_string()}} classes={classes!("main__chat-aside")}>
                    <img src={dialog.avatar_path.clone()}/>
                    <div class="main__text">
                        {dialog.username.clone().to_string()}
                    </div>
                </Link<Route>>
            }       
        );
       
    }
    html!{
        <aside style="padding: 1vh .5vw 6vh .4vw;" class={classes!("main__aside")}>
            <nav class={classes!("main__navbar")}>
                <div class={classes!("main__search")}>
                    <Svg name={SvgIcons::Search}/>
                    <input type="text"/>
                </div>
                
                {dialogs_vnode}
                
            </nav>
        </aside>
    }
}