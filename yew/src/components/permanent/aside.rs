use yew::prelude::*;
use yew_router::prelude::*;
use serde::Deserialize;
use gloo_net::http::Request;
use web_sys::window;
use crate::route::Route;
use crate::elements::svg::{Svg, SvgIcons};
use reqwest;

#[function_component]
pub fn Aside() -> Html {
    let local_storage = window().unwrap().local_storage().unwrap().unwrap();
    let navigator = use_navigator().unwrap();
    let onclick_clear_localstorage = Callback::from(move |e: MouseEvent| {
        window().unwrap().local_storage().unwrap().unwrap().clear();
        window().unwrap().location().reload();
        navigator.push(&Route::Home);
    });
    let avatar = if local_storage.get_item("avatar_path").unwrap().is_some(){
        local_storage.get_item("avatar_path").unwrap().unwrap()
    } else {
        String::from("none")
    };
    
    let aside = match local_storage.get_item("username").unwrap(){
        Some(u) => {
            let ideology_avatar = match local_storage.get_item("ideology").unwrap().unwrap().as_str(){
                "1" => html!{<Svg name={SvgIcons::Libertarian} class="main__position-img"/>},
                "2" => html!{<Svg name={SvgIcons::Communism} class="main__position-img"/>},
                "3" => html!{<Svg name={SvgIcons::Anarchism} class="main__position-img"/>},
                "4" => html!{<Svg name={SvgIcons::Liberalism} class="main__position-img"/>},
                _ => html!{}
            };
            html!{
                <aside class={classes!("main__aside")}>
                    <div class={classes!("main__user")}>
                        <Link<Route> to={Route::Account}>
                            <div class={classes!("main__user-avatar")}>
                                <img src={avatar} alt="" class={classes!("main__user-img")}/>
                                {ideology_avatar}
                            </div>
                            <div class={classes!("main__username")}>{u.clone()}</div>
                        </Link<Route>>
                    </div>
                    <nav class={classes!("main__navbar")}>
                        <Link<Route> classes={classes!("main__link-aside")} to={Route::Home}><Svg name={SvgIcons::Home}/>{"Главная"}</Link<Route>>
                        <Link<Route> classes={classes!("main__link-aside")} to={Route::Compositions}><Svg name={SvgIcons::Compositions}/>{"Книги и статьи"}</Link<Route>>
                        <Link<Route> classes={classes!("main__link-aside")} to={Route::Friends}><Svg name={SvgIcons::Friends}/>{"Друзья"}</Link<Route>>
                        <Link<Route> classes={classes!("main__link-aside")} to={Route::Chats {chat_id: "0".to_owned()}}><Svg name={SvgIcons::Chats}/>{"Чаты"}</Link<Route>>
                        <button onclick={onclick_clear_localstorage} class={classes!("main__btn-aside")}><Svg name={SvgIcons::Exit}/>{"Выход"}</button>
                    </nav>
                </aside>
            }
        },
        None => html!{
            <aside class={classes!("main__aside")}>
                <div class={classes!("main__user")}>
                    <Link<Route> to={Route::Login}>
                        <div class={classes!("main__user-avatar")}>
                            <img src="https://upload.wikimedia.org/wikipedia/commons/e/e5/Karl_Marx%2C_1875.jpg" alt="" class={classes!("main__user-img")}/>
                        </div>
                        <div class={classes!("main__username")}>
                        
                        {"Войти"}</div>
                    </Link<Route>>
                </div>
                <nav class={classes!("main__navbar")}>
                    <Link<Route> classes={classes!("main__link-aside")} to={Route::Home}><Svg name={SvgIcons::Home}/>{"Главная"}</Link<Route>>
                    <Link<Route> classes={classes!("main__link-aside")} to={Route::Compositions}><Svg name={SvgIcons::Compositions}/>{"Книги и статьи"}</Link<Route>>
                    </nav>
            </aside>
        },
    };
    html!{
        {aside}
    }
}