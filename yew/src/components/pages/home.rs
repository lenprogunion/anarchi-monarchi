use yew::{prelude::*, functional::use_state};
use yew_router::prelude::Link;
use crate::components::elements::{block::Block};
use crate::components::permanent::{aside::Aside};
use wasm_bindgen_futures;
use gloo_net::http::Request;
use serde_json;
use crate::route::Route;
use web_sys::window;
#[function_component]
pub fn Home() -> Html {
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Главная"));
    let commit = use_state(|| None);
    let commit_clone = commit.clone();
    use_effect_with_deps(move |_| {
        
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get("https://gitlab.com/api/v4/projects/38541368/repository/branches")
                .await.unwrap()
            
                .text().await.unwrap();
            commit.set(
                Some(
                    serde_json::from_str::<serde_json::Value>(&res)
                    .expect("error json")[0]["commit"]
                    .clone()
                )
            );        
        });
        
    },
    ());
  
    html!{
        <main class="main">
            <Aside/>
            <div class="main__container">
                {match commit_clone.as_ref(){
                    Some(c) => html!{
                        <Block title="Обновления">
                            <div class="main__text">
                                {c["title"].clone()}
                            </div>
                            <div class="main__subtext">
                                {c["message"].clone()}
                            </div>
                        </Block>
                     },
                    None => html!{
                        <Block title="Обновления">
                            <div class="main__text">
                                {"Загрузка..."}
                            </div>
                        </Block>
                    },
                }}
                
                <Block title="Прогресс">
                    <div class="main__text">
                        {"Урок 1"}
                    </div>
                    <div class="main__subtext">
                        {"Часть 1. Вступление"}
                    </div>
                    <div class="main__progress">
                        <Link<Route> to={Route::Lesson{lesson_id: "1".to_owned()}} classes="main__result">
                        </Link<Route>>
                        <Link<Route> to={Route::Lesson{lesson_id: "2".to_owned()}} classes="main__result">
                        </Link<Route>>
                        <Link<Route> to={Route::Lesson{lesson_id: "3".to_owned()}} classes="main__result">
                        </Link<Route>>
                        <Link<Route> to={Route::Lesson{lesson_id: "4".to_owned()}} classes="main__result">
                        </Link<Route>>
                        <Link<Route> to={Route::Lesson{lesson_id: "5".to_owned()}} classes="main__result">
                        </Link<Route>>
                        <Link<Route> to={Route::Lesson{lesson_id: "6".to_owned()}} classes="main__result">
                        </Link<Route>>
                    </div>
                </Block>
            </div>
        </main>
    }
}