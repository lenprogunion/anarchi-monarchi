use std::ptr::eq;

use yew::prelude::*;
use web_sys::{window,HtmlInputElement};
use crate::components::permanent::{chats_aside::Aside};
use crate::elements::{
    svg::{Svg, SvgIcons},
    drop_down_menu::DropDownMenu,
};

#[derive(Clone, serde::Deserialize)]
pub struct FullUserInfoChat {
    id: i32,
    username: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
    text: String,
}

#[derive(PartialEq, Properties)]
pub struct LessonProps {
    pub chat_id: String,
}

#[function_component]
pub fn Chats(props: &LessonProps) -> Html {
    let user_id = window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap();
    if user_id.is_none(){
        return html!{
            <yew_router::prelude::Redirect<crate::route::Route> to={crate::route::Route::Home}/>
        };
    }
    
    let dialog_id = props.chat_id.clone();
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Чаты"));
    let friend_dialog_handle = use_state(crate::FullUserInfo::default);
    let friend_dialog = (*friend_dialog_handle).clone();
    let form_data = [
            ("id_dialog", dialog_id.clone()), 
            ("id_user", window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap().unwrap()),
    ];
    let chat = dialog_id.clone();
    use_effect_with_deps(move |_| {
        if chat != "0".to_owned(){
            wasm_bindgen_futures::spawn_local(async move{
                let res = reqwest::Client::new().post("http://localhost:8086/api/show_dialog_friend")
                    .form(&form_data)
                    .send()
                    .await.unwrap()
                    .json::<crate::FullUserInfo>().await.unwrap();
                match Some(res){
                    Some(u) => {
                        friend_dialog_handle.set(u);
                    },
                    None =>{

                    }
                }
            });
        }
    },
    props.chat_id.clone());
    
    let messages_handle = use_state(|| Vec::<FullUserInfoChat>::new());
    let messages = (*messages_handle).clone();
    let drop_down_menu = use_state(|| "hidden");
    let onclick = {
        let drop_down_menu = drop_down_menu.clone();    
        
        Callback::from(move |_| {
            if *drop_down_menu == "hidden"{
                drop_down_menu.set("visible");
            }
            else{
                drop_down_menu.set("hidden");
            }
        })
    };
    let mut dialogs_vnode = Vec::<Html>::new();
    if dialog_id.clone() != "0".to_owned(){
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get(&format!("http://localhost:8086/api/show_messages/{}", dialog_id.clone()))
                .await.unwrap()
                .json::<Vec<FullUserInfoChat>>().await.unwrap();
            match Some(res){
                Some(m) => {
                    messages_handle.set(m);
                },
                None =>{

                }
            }          
        });

      
        for message in &messages{
            if message.id.to_string() == user_id.clone().unwrap(){
                dialogs_vnode.push(
                    html!{
                        <div class={classes!("user__message")}>
                            <img title={message.username.clone()} src={message.avatar_path.clone()}/>
                            <div class={classes!("user__message-text")}>
                                {message.text.clone()}
                            </div>
                        </div>
                    }
                )
            } else{
                dialogs_vnode.push(
                    html!{
                        <div class={classes!("friend__message")}>
                            <img title={message.username.clone()} src={message.avatar_path.clone()}/>
                            <div class={classes!("friend__message-text")}>
                                {message.text.clone()}
                            </div>
                        </div>
                    }
                )
            }
            
        }
    }
    let ideology_avatar = match friend_dialog.ideology.to_string().as_str(){
        "1" => html!{<Svg name={SvgIcons::Libertarian}/>},
        "2" => html!{<Svg name={SvgIcons::Communism}/>},
        "3" => html!{<Svg name={SvgIcons::Anarchism}/>},
        "4" => html!{<Svg name={SvgIcons::Liberalism}/>},
        _ => html!{}
    };
    let dialog_id_for_send = props.chat_id.clone();
    let onchange_message = Callback::from(move |e: KeyboardEvent| {
        let input = e.target_dyn_into::<HtmlInputElement>();
        if let Some(input) = input {

            if e.key() == "Enter" {
                let send_message_form = [
                    ("text", input.value().clone()),
                    ("user_id", user_id.clone().unwrap()),
                    ("dialog_id", dialog_id_for_send.clone()), 
                ];
                wasm_bindgen_futures::spawn_local(async move{
                    reqwest::Client::new().post("http://localhost:8086/api/send_message")
                        .form(&send_message_form)
                        .send()   
                        .await.unwrap();     
                });
                input.set_value("");
            }
        }
    });

    html!{
        <main class="main">
            <Aside/>
            <div class="main__container chat">
            if props.chat_id.clone() != "0".to_owned(){
                <div class={classes!("main__titlebar-chat")}>
                
                    <div class={classes!("main__user-chat")}>
                        <img src={friend_dialog.avatar_path.clone()}/>
                        <div class="main__text">
                            {friend_dialog.username.clone()}
                        </div>  
                        {ideology_avatar}
                    </div>
                    <div class={classes!("main__toolbar-chat")}>
                        <button class={classes!("main__user-info-chat")}>
                            <Svg name={SvgIcons::Profile}/>
                        </button>
                        <button {onclick} class={classes!("main__actions-chat")}>
                            <Svg name={SvgIcons::More}/>
                        </button>
                            <DropDownMenu state={*drop_down_menu} place="chats-toolbar">
                                <div></div>
                            </DropDownMenu>
                    </div>
                
                </div> 
                <div class={classes!("main__chat")}>
                    <div class={classes!("users__messages")}>
                        {dialogs_vnode}
                    </div>
                    
                    <div class={classes!("message-bar")}>
                        <button class={classes!("")}>
                            <Svg name={SvgIcons::Clip}/>
                        </button>
                        <input onkeypress={onchange_message.clone()} class={classes!("main__message-box")}/>
                        <button class={classes!("")}>
                            <Svg name={SvgIcons::Smile}/>
                        </button>
                        <button class={classes!("")}>

                        </button>
                    </div>
                </div>
            }
            </div>
        </main>

          

    }
}