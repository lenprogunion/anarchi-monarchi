use yew::prelude::*;
use yew_router::prelude::*;
use crate::components::elements::{block::Block};
use crate::components::permanent::{aside::Aside};
use web_sys::{window, HtmlInputElement};
use crate::Route;

#[function_component]
pub fn Login() -> Html {
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Авторизация"));
    let local_storage = window().unwrap().local_storage().unwrap().unwrap();
    let error_handle = use_state(String::default);
    let error = (*error_handle).clone();
    let navigator = use_navigator().unwrap();
    let username_handle = use_state(String::default);
    let username = (*username_handle).clone();
    let password_handle = use_state(String::default);
    let password = (*password_handle).clone();

    let onchange_username = Callback::from(move |e: Event| {
        let input = e.target_dyn_into::<HtmlInputElement>();

        if let Some(input) = input {
            username_handle.set(input.value());
        }
    });
    let onchange_password = Callback::from(move |e: Event| {
        let input = e.target_dyn_into::<HtmlInputElement>();

        if let Some(input) = input {
            password_handle.set(input.value());
        }
    });
    let onclick_auth = {
        let form_data = [("username", username.clone()), ("password", password.clone())];
        Callback::from(move |e: MouseEvent| {
            let error_handle = error_handle.clone();
            
            let local_storage = local_storage.clone();
            let navigator = navigator.clone();
            let form_data = form_data.clone();
            let client = reqwest::Client::new();
            wasm_bindgen_futures::spawn_local(async move{
                let res = client.post("http://localhost:8086/login")
                    .form(&form_data)
                    .send()
                    .await.expect("asdas")
                    .json::<crate::FullUserInfo>().await;
                if res.is_err(){
                    error_handle.set(String::from("Неправильный логин или пароль"));
                }
                match Some(res.unwrap()){
                    Some(u) => {
                        let ideology_avatar = match u.ideology.clone().to_string().as_str(){
                            "1" => "https://cs13.pikabu.ru/post_img/big/2020/12/17/12/160823873811438984.jpg",
                            "2" => "https://c0.klipartz.com/pngpicture/187/432/gratis-png-ilustracion-de-logotipo-de-hoz-y-hacha-roja-union-sovietica-de-martillo-y-comunismo-simbolismo-comunista-logotipo-de-union-sovietica.png",
                            "3" => "https://www.clker.com/cliparts/J/O/V/Z/e/5/anarcho-communism-hi.png",
                            "4" => "https://aliterabogados.com/wp-content/uploads/2014/12/abogados.png",
                            _ => "None"
                        };
                        local_storage.set_item("id", &u.id.to_string());
                        local_storage.set_item("username", &u.username);
                        local_storage.set_item("email", &u.email);
                        local_storage.set_item("ideology", &u.ideology.to_string());
                        local_storage.set_item("password", &u.password.to_string());
                        if u.full_name.is_some(){
                            local_storage.set_item("full_name", &u.full_name.unwrap());
                        }
                        
                        local_storage.set_item("avatar_path", &format!("../avatars/{}", local_storage.get_item("id").unwrap().unwrap()));
                        
                        
                        local_storage.set_item("ideology_avatar", ideology_avatar);
                        navigator.push(&Route::Home)
                    }, 
                    None => {
                        error_handle.set(String::from("Неправильный логин или пароль"));
                    }
                };
                
            });
        })
    };
    
    html!{
        {match window().unwrap().local_storage().unwrap().unwrap().get_item("username").unwrap(){
            Some(u) => html!{
                <Redirect<Route> to={Route::Home}/>
            },
            None => html!{
                <main class="main">
                    <Aside/>
                    <div class="main__container">
                        <div class="main__title">
                        {"Авторизация"}
                        </div>
                        <form class="main__form"> 
                            {error}
                            <input class="main__input" onchange={onchange_username} value={username.clone()} placeholder="Имя пользователя..." type="text" name="username" maxlength="150" autofocus=true required=true id="id_username"/>
                            <input class="main__input" onchange={onchange_password} value={password.clone()}placeholder="Пароль..." type="password" name="password" autocomplete="new-password" required=true id="id_password"/>
                            <button class="main__btn-form" type="button" onclick={onclick_auth}>{"Авторизироваться"}</button>
                            <Link<Route> to={Route::Register}>{"Зарегистрироваться"}</Link<Route>>
                        </form>
                    </div>
                </main>
            }
        }
        }
    }
            
    
}