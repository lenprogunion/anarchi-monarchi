use yew::prelude::*;

use crate::components::elements::{block::Block};
use crate::components::permanent::{aside::Aside};
use web_sys::window;
#[function_component]
pub fn Register() -> Html {
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Регистрация"));

    html!{
        {match window().unwrap().local_storage().unwrap().unwrap().get_item("username").unwrap(){
            Some(u) => html!{
                <yew_router::prelude::Redirect<crate::route::Route> to={crate::route::Route::Home}/>
            },
            None => html!{
        <main class="main">
            <Aside/>
            <div class="main__container">
                <div class="main__title">
                {"Регистрация"}
                </div> 
                <form class="main__form" action="http://localhost:8086/register" method="post">            
                    <input class="main__input" placeholder="Имя пользователя..." class="" type="text" name="username" maxlength="150" autofocus=true required=true id="id_username"/>
                    <input class="main__input" placeholder="Email..." type="email" name="email" minlength="8" required=true id="id_email"/>
                    <input class="main__input" placeholder="Пароль..." type="password" name="password" minlength="8" autocomplete="new-password" required=true id="id_password1"/>
                    <input class="main__input" placeholder="Повторите пароль..." type="password" name="password_confirm" minlength="8" autocomplete="new-password" required=true id="id_password1"/>
                    <select class="main__input" name="ideology" required=true id="id_ideology">
                        <option value="" selected=true>{"Выберете идеологию..."}</option>
                        <option value="1">{"Либертарианство"}</option>
                        <option value="2">{"Марксизм"}</option>
                        <option value="3">{"Анархия"}</option>
                        <option value="4">{"Либерализм"}</option>
                    </select>
                    <button class="main__btn-form" type="submit">{"Зарегистрироваться"}</button>
                </form>
            </div>
        </main>
    }}}
    }
}