use yew::prelude::*;
use yew_router::prelude::*;
use std::{fs::File, io::Read};
use crate::route::Route;
use crate::components::elements::{
    block::Block, 
    card::{CardForm, Card}, 
    svg::{Svg, SvgIcons}
};
use web_sys::{HtmlInputElement, window, FileReader};
use reqwest;
use reqwest::multipart::{Form, Part};
#[function_component]
pub fn Account() -> Html {
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Личный кабинет"));
    let local_storage = window().unwrap().local_storage().unwrap().unwrap();
    let user_id = local_storage.get_item("id").unwrap();
    if user_id.is_none(){
        return html!{
            <Redirect<Route> to={Route::Home}/>
        }
    }
   
    let username = local_storage.get_item("username").unwrap().unwrap();
    let email = local_storage.get_item("email").unwrap().unwrap();
    let full_name = local_storage.get_item("full_name").unwrap();
    let avatar_path = local_storage.get_item("avatar_path").unwrap().unwrap();
   
    let ideology_avatar = match local_storage.get_item("ideology").unwrap().unwrap().as_str(){
        "1" => html!{<Svg name={SvgIcons::Libertarian} class="main__position_account-img"/>},
        "2" => html!{<Svg name={SvgIcons::Communism} class="main__position_account-img"/>},
        "3" => html!{<Svg name={SvgIcons::Anarchism} class="main__position_account-img"/>},
        "4" => html!{<Svg name={SvgIcons::Liberalism} class="main__position_account-img"/>},
        _ => html!{}
    };
    
    let ideology = match local_storage.get_item("ideology").unwrap().unwrap().as_str(){
        "1" => "Либертарианство",
        "2" => "Коммунизм",
        "3" => "Анархизм",
        "4" => "Либерализм",
        _ => "Ошибка",
    };
    let favorite_compositions_handle = use_state(|| None);
    let favorite_compositions = (*favorite_compositions_handle).clone();
    let mut favorite_compositions_vnode = Vec::<Html>::new();
  
    let favorite_compositions_handle = favorite_compositions_handle.clone();
    use_effect_with_deps(move |_| {
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get(&format!("http://localhost:8086/api/favorite_compositions/{}", user_id.expect("Nan").to_string()))
                .await.unwrap()
                .json::<Vec<crate::pages::compositions::Composition>>().await.unwrap();
            
            favorite_compositions_handle.set(Some(res));
            
        });
    },
    ());
    
    if favorite_compositions.is_some(){
        for composition in &favorite_compositions.unwrap(){
            favorite_compositions_vnode.push(html!{
                <a class="main__card">
                    <div class="main__composition">
                        <img src={composition.image.clone()}/>
                        <div class="main__text">
                            {composition.name.clone()}
                            <div class="main__subtext">
                                {composition.author.clone()}
                            </div>
                        </div>
                    </div>
                    <button data-id={composition.id.clone().to_string()} class="main__svg_composition">
                        <Svg name={SvgIcons::AddPlus}/>
                    </button>
                </a>
            });
        }
    }
    let user_id = local_storage.get_item("id").unwrap().unwrap();
    let onchange_image = Callback::from(move |e: Event| {
        let input = e.target_dyn_into::<HtmlInputElement>();
        if let Some(input) = input {
            input.form().unwrap().submit();
        }
    });
    let user_id = local_storage.get_item("id").unwrap().unwrap();
    let navigator = use_navigator().unwrap();
    let delete_account = Callback::from(move |_| {
        let navigator = navigator.clone();
        let user_id = user_id.clone();
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get(&format!("http://localhost:8086/api/delete_user/{}", user_id.clone()))
                .await.unwrap();
        });
       
        window().unwrap().local_storage().unwrap().unwrap().clear();
        navigator.push(&Route::Home);
        
    });
    let onclick_try_delete_account = Callback::from(move |e: MouseEvent| {
        let prompt = window().unwrap().prompt_with_message("Если вы хотите удалить аккаунт, введите пароль").unwrap().unwrap();
        if prompt == window().unwrap().local_storage().unwrap().unwrap().get_item("password").unwrap().unwrap(){
            delete_account.emit("");
        }
    });
    let user_id = local_storage.get_item("id").unwrap().unwrap();
    html!{
    
        <main class="main">
            <div class="main__user_account">
                <form action={format!("http://localhost:8086/upload_avatar/{}", user_id.clone())} enctype="multipart/form-data" method="POST">
                    <input onchange={onchange_image} name="file-input"  accept=".jpg, .jpeg, .png" id="file-input" type="file"/>
                    <label for="file-input" class="main__user_account-avatar">
                        <div alt="" class="main__user_account-img">{"Изменить фото профиля"}</div>
                        <img src={avatar_path.clone()} alt="" class="main__user_account-img"/>
                        {ideology_avatar.clone()}
                    </label>
                </form>
                    if full_name.is_some(){ 
                        <div class="main__username_account">{full_name.unwrap()}</div>
                        <div class="main__subtext">{format!("@{}", username.clone())}</div>
                    } else {
                        <div class="main__username_account">{format!("@{}", username.clone())}</div>
                    }
                   
            </div>
            <div class={classes!("main__container", "account")} >
                <Block title="Профиль">
                    <CardForm title="имя" db_name="name" input_type="text"/>
                    <Card icon={avatar_path.clone()} title="своё имя" db_name="name" db_value={username}/>
            
                    <div class="main__card main__user_ideology">
                        <form class="main__change-form" action="" method="post">
                            <select class="main__input" name="ideology" required=true id="id_ideology">
                                <option value="" selected=true>{"---------"}</option>
                                <option value="{{ideology.id}}">{"Анархия"}</option>
                            </select>
                                <button class="main__btn-form">{"Изменить"}</button>
                        </form>
                    </div>
                    <Card icon={local_storage.get_item("ideology").unwrap().unwrap()} title="свою идеологию" db_name="ideology" db_value={ideology}/>
                        
                    <CardForm title="email" db_name="email" input_type="email"/>
                    <Card icon="https://cdn-icons-png.flaticon.com/512/3059/3059486.png " title="свой email" db_name="email" db_value={email}/>
                </Block>
                if !favorite_compositions_vnode.is_empty(){
                    <Block title="Избранное">
                        {favorite_compositions_vnode}
                    </Block>
                }
                <button onclick={onclick_try_delete_account} class="main__text" style="cursor:pointer;color: #C01C28">
                    {"Удалить аккаунт"}
                </button>
            </div>
        </main>
       
    }
}