use yew::prelude::*;

use crate::components::{
    elements::{block::Block},
    permanent::{aside::Aside},
};
use crate::elements::svg::{Svg, SvgIcons};
use web_sys::{window,HtmlInputElement, HtmlElement};

#[function_component]
pub fn Friends() -> Html {
    
    let user_id = window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap();
    if user_id.is_none(){ 
        return html!{
            <yew_router::prelude::Redirect<crate::route::Route> to={crate::route::Route::Home}/>
        };
    }
    let users_handle = use_state(|| Vec::<crate::FullUserInfo>::new());
    let users = (*users_handle).clone();
    let friends_handle = use_state(|| Vec::<crate::FullUserInfo>::new());
    let friends = (*friends_handle).clone();
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Ваши друзья"));
    use_effect_with_deps(move |_| {
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get("http://localhost:8086/api/users")
                .await.unwrap()
            
                .json::<Vec<crate::FullUserInfo>>().await.unwrap();
            match Some(res){
                Some(u) => {
                    users_handle.set(u);
                },
                None =>{

                }
            }          
        });
    },
    user_id.clone());

    
    wasm_bindgen_futures::spawn_local(async move{
        let res = reqwest::get(&format!("http://localhost:8086/api/friends/{}", window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap().unwrap()))
            .await.unwrap()
        
            .json::<Vec<crate::FullUserInfo>>().await.unwrap();
        match Some(res){
            Some(u) => {
                friends_handle.set(u);
            },
            None =>{

            }
        }          
    });


    let search_handle = use_state(String::default);
    let search = (*search_handle).clone();
    let users_result_handle = use_state(|| Vec::<crate::FullUserInfo>::new());
    let users_result = (*users_result_handle).clone();
    let onchange_search = Callback::from(move |e: InputEvent| {
        let input = e.target_dyn_into::<HtmlInputElement>();
        if let Some(input) = input {
            search_handle.set(input.value().clone());
            let mut search_users = Vec::<crate::FullUserInfo>::new();
            for user in &users{
                if user.username.contains(&input.value().clone()){
                    search_users.push(user.clone());
                }
            }
            users_result_handle.set(search_users);
        }
    });
    let onclick_add_friend = Callback::from(move |e: MouseEvent| {
        let button = e.target_dyn_into::<HtmlElement>().expect("It's not button?").dataset().get("id").unwrap();
        let id = window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap().unwrap();
        let add_friend_form_data = [
            ("id_user", id.clone()), 
            ("id_friend", button.clone()),
        ];
        let create_dialog_form_data = [
            ("name", "First dialog".to_string()),
            ("image", "".to_string()),
            ("user_id", id.clone()), 
            ("friend_id", button.clone()),
        ];
        wasm_bindgen_futures::spawn_local(async move{
            reqwest::Client::new().post("http://localhost:8086/api/add_friend")
                .form(&add_friend_form_data)
                .send()   
                .await.unwrap();     
            reqwest::Client::new().post("http://localhost:8086/api/create_friend_dialog")
                .form(&create_dialog_form_data)
                .send()   
                .await.unwrap();  
        });
    });
    let mut friends_vnode = Vec::<Html>::new();
    for user in &friends.clone(){
        if user_id.clone().unwrap() == user.id.to_string(){
            continue;
        }

        let ideology_avatar = match user.ideology.clone().to_string().as_str(){
            "1" => html!{<Svg name={SvgIcons::Libertarian} class="main__svg_ideology"/>},
            "2" => html!{<Svg name={SvgIcons::Communism} class="main__svg_ideology"/>},
            "3" => html!{<Svg name={SvgIcons::Anarchism} class="main__svg_ideology"/>},
            "4" => html!{<Svg name={SvgIcons::Liberalism} class="main__svg_ideology"/>},
            _ => html!{}
        };
        friends_vnode.push(html!{
            <div class="main__card">
                <div class="main__composition">
                    <img src={user.avatar_path.clone()}/>
                    <div class="main__text">
                        {user.username.clone()}
                    </div>
                </div>
                <div class="main__svg_composition">
                    {ideology_avatar}
                    <Svg name={SvgIcons::FrontArrow}/>
                </div>
            </div>
        });
    }

    let mut search_vnode = Vec::<Html>::new();
    'tens: for user in &users_result{
        if user_id.clone().unwrap() == user.id.to_string(){
            continue;
        }
        for friend in &friends.clone(){
            if user.id.clone() == friend.id.clone(){continue 'tens;}
        }
        let ideology_avatar = match user.ideology.clone().to_string().as_str(){
            "1" => html!{<Svg name={SvgIcons::Libertarian} class="main__svg_ideology"/>},
            "2" => html!{<Svg name={SvgIcons::Communism} class="main__svg_ideology"/>},
            "3" => html!{<Svg name={SvgIcons::Anarchism} class="main__svg_ideology"/>},
            "4" => html!{<Svg name={SvgIcons::Liberalism} class="main__svg_ideology"/>},
            _ => html!{}
        };
        search_vnode.push(html!{
            <button onclick={onclick_add_friend.clone()} data-id={user.id.clone().to_string()} class="main__card">
                <div class="main__composition">
                    <img src={user.avatar_path.clone()}/>
                    <div class="main__text">
                        {user.username.clone()}
                    </div>
                </div>
                <div class="main__svg_composition">
                    {ideology_avatar}
                    <Svg name={SvgIcons::AddPlus}/>
                </div>
            </button>
        });
    }
    html!{
        <main class="main">
            <Aside/>
            <div class="main__container">
                <div class={classes!("main__search")}>
                    <Svg name={SvgIcons::Search}/>
                    <input oninput={onchange_search} type="text"/>
                </div>
                if search.clone().is_empty(){
                    
                    if friends_vnode.is_empty(){
                        <Block title="Друзья">
                            <span></span>
                        </Block>
                        <div class="main__text">{"Нет друзей"}</div> 
                    }else{
                        <Block title="Друзья">
                            {friends_vnode}
                        </Block>
                    }
                } else{
                    if search_vnode.is_empty(){
                        <Block title="Поиск">
                            <span></span>
                        </Block>
                        <div class="main__text">{"Пользователь не найден"}</div> 
                    } else {
                        <Block title="Поиск">
                            {search_vnode}
                        </Block>
                    }
                }
            </div>
        </main>
    }
}