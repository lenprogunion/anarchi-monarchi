use yew::prelude::*;

use crate::components::elements::{block::Block};
use crate::components::permanent::{aside::Aside};
use crate::components::elements::svg::{Svg, SvgIcons};
use web_sys::{window, DomStringMap, HtmlElement};

#[derive(Clone, PartialEq, serde::Deserialize, Default)]
pub struct Composition{
    pub id: i32,
    pub name: String,
    pub description: String,
    pub author: String,
    pub image: String,
    pub type_id: i32, 
    pub ideology: i32,
}


#[function_component]
pub fn Compositions() -> Html {
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some("Книги и статьи"));
    
    let mut ideology_id = window().unwrap().local_storage().unwrap().unwrap().get_item("ideology").unwrap();
    let user_id = window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap();
    if ideology_id.is_none(){
        ideology_id = Some(String::from("5"));
    }
    let favorite_compositions_handle = use_state(|| None);
    let favorite_compositions = (*favorite_compositions_handle).clone();
    let user_id_clone = user_id.clone();
    let mut favorite_compositions_vnode = Vec::<Html>::new();
 
    if user_id.is_some(){
        
        let user_id = user_id.clone();
        let favorite_compositions_handle = favorite_compositions_handle.clone();
        wasm_bindgen_futures::spawn_local(async move{
            match reqwest::get(&format!("http://localhost:8086/api/favorite_compositions/{}", user_id.expect("Nan").to_string()))
                .await{
                    Ok(r) => {
                        favorite_compositions_handle.set(Some(r.json::<Vec<Composition>>().await.unwrap()));
                    },
                    Err(r) =>{
                        
                    }
                }
            
        });
     
        if favorite_compositions.is_some(){
            for composition in &favorite_compositions.clone().unwrap(){
                favorite_compositions_vnode.push(html!{
                    <a  class="main__card">
                        <div class="main__composition">
                            <img src={composition.image.clone()}/>
                            <div class="main__text">
                                {composition.name.clone()}
                                <div class="main__subtext">
                                    {composition.author.clone()}
                                </div>
                            </div>
                        </div>
                        <button data-id={composition.id.clone().to_string()} class="main__svg_composition">
                            <Svg name={SvgIcons::FrontArrow}/>
                        </button>
                    </a>
                });
            }
        }
    }
    let compositions_handle = use_state(|| Vec::<Composition>::new());
    let compositions = (*compositions_handle).clone();
    use_effect_with_deps(move |_| {
  
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::get(&format!("http://localhost:8086/api/compositions/{}", ideology_id.expect("Nan").to_string()))
                .await.unwrap()
            
                .json::<Vec<Composition>>().await.unwrap();
            match Some(res){
                Some(c) => {
                    compositions_handle.set(c);
                },
                None =>{

                }
            }          
        });
        
    },
    ());
    let onclick_add_favorite = Callback::from(move |e: MouseEvent| {
        let button = e.target_dyn_into::<HtmlElement>().expect("It's not button?").dataset().get("id").unwrap();
        let form_data = [
            ("user_id", window().unwrap().local_storage().unwrap().unwrap().get_item("id").unwrap().unwrap()), 
            ("composition_id", button.clone()),
        ];
       
        wasm_bindgen_futures::spawn_local(async move{
            let res = reqwest::Client::new().post("http://localhost:8086/api/add_favorite")
                .form(&form_data)
                .send()   
                .await.unwrap();     
        });
    });
    let mut vnode = Vec::<Html>::new();
    'tens: for composition in &compositions{
        if favorite_compositions.is_some(){
            for favorite in &favorite_compositions.clone().unwrap(){
                if composition.id.clone() == favorite.id.clone(){continue 'tens;}
            }
        }
        vnode.push(html!{
            <div class="main__card">
                <div class="main__composition">
                    <img src={composition.image.clone()}/>
                    <div style="text-align:left;"  class="main__text">
                        {composition.name.clone()}
                        <div class="main__subtext">
                            {composition.author.clone()}
                        </div>
                    </div>
                </div>
                if user_id_clone.clone().is_some() {
                    <button  onclick={onclick_add_favorite.clone()} data-id={composition.id.clone().to_string()}  class="main__svg_composition">
                        <Svg name={SvgIcons::AddPlus}/>
                    </button>
                } else {
                    <div class="main__svg_composition">
                        <Svg name={SvgIcons::FrontArrow}/>
                    </div>
                }
            </div>
        });
    }


    html!{
        <main class="main">
            <Aside/>
            <div class="main__container">
                if user_id_clone.is_some() && !favorite_compositions_vnode.is_empty(){
                    <Block title="Избранное">
                        {favorite_compositions_vnode}
                    </Block>
                }
                if !vnode.is_empty(){
                    <Block title="Книги">
                        {vnode}
                    </Block>
                }
                
            </div>
        </main>
    }
}