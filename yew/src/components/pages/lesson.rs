use yew::{prelude::*,functional::use_state};
use yew_router::utils::fetch_base_url;
use crate::ahtml::ahtml_parse;
use crate::components::permanent::aside::Aside;
use gloo_net::http::Request;
use wasm_bindgen_futures;
use web_sys::window;
#[derive(PartialEq, Properties)]
pub struct LessonProps {
    pub lesson_id: String,
}

#[function_component]
pub fn Lesson(props: &LessonProps) -> Html{
    let lesson_id = props.lesson_id.clone();
    let title = window().unwrap().document().unwrap().get_elements_by_tag_name("title").item(0).unwrap();
    title.set_text_content(Some(&format!("Урок {}", lesson_id.clone())));
    let childs= use_state(|| None);
    let childs_clone = (*childs).clone();

    wasm_bindgen_futures::spawn_local(async move{
        let resp = Request::get(&format!("http://localhost:8086/api/lesson/{}", lesson_id))
        .send()
        .await.unwrap()     
        .text()
        .await
        .unwrap();
        
        childs.set(Some(ahtml_parse::parser(resp)));
    });

    html!{
        <main class="main">
            <Aside/>
            <div class="main__container">
            
            if childs_clone.is_some(){
                {childs_clone.unwrap()}
            } else{
                {"Загрузка..."}
            }
            
            </div>
        </main>
    }
}