use yew::prelude::*;
use crate::elements::svg::{Svg, SvgIcons};

#[derive(Properties, PartialEq)]
pub struct CardFormProps {
    pub title: AttrValue,
    pub db_name: AttrValue,
    pub input_type: AttrValue,
 
}

#[function_component]
pub fn CardForm(props: &CardFormProps) -> Html {
    html!{
        <div class={classes!("main__card",  format!("main__user_{}", props.db_name.clone()))}>
            <form class="main__change-form" action="" method="post">

                <input type={props.input_type.clone()} required=true name={props.db_name.clone()} placeholder={format!("Введите {}...", props.title.clone())} class="main__input"/>
                
                 <button class="main__btn-form">{"Изменить"}</button>
               
            </form>
        </div>
    }
}

#[derive(Properties, PartialEq)]
pub struct CardProps {
    pub title: AttrValue,
    pub db_name: AttrValue,
    pub db_value: AttrValue,
    pub icon: String,
}
#[function_component]
pub fn Card(props: &CardProps) -> Html {
    let ideology_avatar = match props.icon.clone().as_str(){
        "1" => Some(html!{<Svg name={SvgIcons::Libertarian} />}),
        "2" => Some(html!{<Svg name={SvgIcons::Communism} />}),
        "3" => Some(html!{<Svg name={SvgIcons::Anarchism} />}),
        "4" => Some(html!{<Svg name={SvgIcons::Liberalism} />}),
        _ => None
    };
    html!{
        
        <div class={classes!("main__card",format!("main__user_{}", props.db_name.clone()),"translate0")}>
            <div class="main__composition">
                if ideology_avatar.is_some(){
                    {ideology_avatar}
                } else{
                    <img src={props.icon.clone()}/>
                }
                
                <div class="main__text">
                    {props.db_value.clone()}
                    <div class="main__subtext">{format!("Нажмите, чтобы изменить {}", props.title.clone())}</div>
                    
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="13" viewBox="0 0 5 8" fill="none"><path d="M4.35355 4.35355C4.54882 4.15829 4.54882 3.84171 4.35355 3.64645L1.17157 0.464466C0.976311 0.269204 0.659729 0.269204 0.464466 0.464466C0.269204 0.659728 0.269204 0.97631 0.464466 1.17157L3.29289 4L0.464466 6.82843C0.269204 7.02369 0.269204 7.34027 0.464466 7.53553C0.659728 7.7308 0.97631 7.7308 1.17157 7.53553L4.35355 4.35355ZM3 4.5L4 4.5L4 3.5L3 3.5L3 4.5Z" fill="#D1D1D1"/></svg>
        </div>
    }
}