use yew::prelude::*;


#[derive(Properties, PartialEq)]
pub struct Props {
    pub state: AttrValue,
    pub place: AttrValue,
    pub children: Children,
}

#[function_component]
pub fn DropDownMenu(props: &Props) -> Html {
    html!{
        <div class={classes!("main__drop_down", props.place.to_string(), props.state.to_string())}>
           {for props.children.iter()}
        </div>
    }
}