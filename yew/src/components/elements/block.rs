use yew::prelude::*;
use yew_router::prelude::use_location;

#[derive(Properties, PartialEq)]
pub struct Props {
    pub title: AttrValue,
    pub children: Children,
}

#[function_component]
pub fn Block(props: &Props) -> Html {
    let location = use_location().unwrap();
    let add_class_location = location.path() == "/account"||location.path() == "/friends" || location.path() == "/compositions";
    html!{
        <div class={classes!("main__block")}>
            <div class={classes!("main__title")}>{props.title.clone()}</div>
            <div class={classes!("main__content-block", if add_class_location {"list"} else{""})}>
                { for props.children.iter() }
            </div>
        </div>
    }
}