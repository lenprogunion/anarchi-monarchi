const email = document.querySelectorAll('.main__user_email');
const birthDay = document.querySelectorAll('.main__user_birthday');
const ideology = document.querySelectorAll('.main__user_ideology');
const username = document.querySelectorAll('.main__user_name');
const surname = document.querySelectorAll('.main__user_surname');

const inputFile = document.querySelector("#file-input");


function translate100(params) {
    params[0].classList.toggle("translate0") 
    params[1].classList.toggle("translate100") 
       
    console.log(params[0])   
}

email.forEach(element => {
    element.addEventListener('click', (e) =>{ 
        if(!e.target.classList.contains('main__input') && !e.target.classList.contains('main__btn-form'))
            translate100(email) 
    })
});

birthDay.forEach(element => { 
    element.addEventListener('click', (e) =>{ 
        if(!e.target.classList.contains('main__input') && !e.target.classList.contains('main__btn-form'))
            translate100(birthDay) 
    })
});

ideology.forEach(element => {
    element.addEventListener('click', (e) =>{ 
        if(!e.target.classList.contains('main__input') && !e.target.classList.contains('main__btn-form'))
            translate100(ideology) 
    })
});

username.forEach(element => {
    element.addEventListener('click', (e) =>{ 
        if(!e.target.classList.contains('main__input') && !e.target.classList.contains('main__btn-form'))
            translate100(username) 
    })
});
surname.forEach(element => {
    element.addEventListener('click', (e) =>{ 
        if(!e.target.classList.contains('main__input') && !e.target.classList.contains('main__btn-form'))
            translate100(surname) 
    })
});
inputFile.addEventListener('input', () => {
    inputFile.form.submit()
})