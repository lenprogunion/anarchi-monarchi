use std::{fs::{File}, io::{Read, Write}};

use actix_cors::Cors;
use actix_web::{
    error::JsonPayloadError, get, post, http,
    web,
    App, Error, HttpResponse, HttpServer, Responder, HttpRequest, HttpMessage, Result,
    http::header::CONTENT_LENGTH
};
use serde::{Deserialize, Serialize};
use actix_multipart::Multipart;
use futures_util::TryStreamExt;
pub mod db;
pub mod middleware;

#[derive(Serialize, Deserialize)]
struct RegisterFormData {
    username: String,
    password: String,
    password_confirm: String,
    email: String,
    ideology: i32,
}

#[derive(Serialize, Deserialize)]
struct LoginFormData {
    username: String,
    password: String,
}
#[derive(Serialize, Deserialize)]
struct FriendsForm {
    id_user: i32,
    id_friend: i32,
}

#[derive(Serialize, Deserialize)]
struct FriendsDialogForm {
    id_user: i32,
    id_dialog: i32,
}

#[derive(Serialize, Deserialize)]
struct DialogForm {
    name: String,
    image: String,
    user_id: i32,
    friend_id: i32,
}

#[derive(Serialize, Deserialize)]
struct MessageForm {
    text: String,
    user_id: i32,
    dialog_id: i32,
}

#[derive(Serialize, Deserialize)]
struct FavoriteForm {
    user_id: i32,
    composition_id: i32,
}

#[get("/api/lesson/{id}")]
async fn lesson(path: web::Path<(u32,)>) -> String {
    
    let mut ahtml = String::new();
    // Добавление первого атрибута адресной в путь к уроку
    let file = File::open(format!("lessons/lesson{}.ahtml", path.into_inner().0));
    if file.is_err(){
        return String::from("Title:\"Not found\"");
    }
    file.expect("msg").read_to_string(&mut ahtml);
    ahtml
   
}
#[get("/api/friends/{id}")]
async fn friends(path: web::Path<(i32,)>) -> impl Responder{
    web::Json(db::interaction::show_friends(path.into_inner().0))
}

#[get("/api/users")]
async fn users() -> impl Responder{
    web::Json(db::interaction::show_users())
}
#[post("/api/add_friend")]
async fn add_friend(form: web::Form<FriendsForm>) -> impl Responder{
    db::interaction::add_friend(form.id_user.clone(), form.id_friend.clone());
    String::from("Added")
}

#[post("/api/create_friend_dialog")]
async fn create_friend_dialog(form: web::Form<DialogForm>) -> impl Responder{
    db::interaction::create_friend_dialog(
        form.name.clone(), 
        form.image.clone(),
        form.user_id.clone(),
        form.friend_id.clone()
    );
    String::from("Created")
}

#[post("/api/show_dialog_friend")]
async fn show_dialog_friend(form: web::Form<FriendsDialogForm>) -> impl Responder{
    web::Json(db::interaction::show_dialog_friend(form.id_dialog.clone(), form.id_user.clone()))
}

#[get("/api/show_dialog/{user_id}")]
async fn show_dialog(path: web::Path<(i32,)>) -> impl Responder{
    web::Json(db::interaction::show_dialog(path.into_inner().0))
}

#[get("/api/show_messages/{dialog_id}")]
async fn show_messages(path: web::Path<(i32,)>) -> impl Responder{
    web::Json(db::interaction::show_messages(path.into_inner().0))
}

#[post("/api/send_message")]
async fn send_message(form: web::Form<MessageForm>) -> impl Responder{
    web::Json(db::interaction::send_message(form.user_id.clone(), form.dialog_id.clone(), form.text.clone()))
}
#[post("/register")]
async fn register(request: HttpRequest, form: web::Form<RegisterFormData>) -> impl Responder {
    let user = db::interaction::create_user(form.username.clone(), form.password.clone());
    db::interaction::create_profile(user.id.clone(), form.email.clone(), form.ideology.clone());
    web::Redirect::to("http://localhost:3000/login").see_other()
}

#[post("/login")]
async fn login(form: web::Form<LoginFormData>) -> Result<impl Responder> {
    let user = db::interaction::check_user(form.username.clone(), form.password.clone());
    match user{
        Some(u) => Ok(web::Json(u)),
        None => Err(Error::from(JsonPayloadError::ContentType)),
    }
}

#[post("/upload_avatar/{user_id}")]
async fn upload_avatar(mut payload: Multipart, path: web::Path<(i32,)>) -> impl Responder {
    let user_id = path.into_inner().0.clone();
    let destination: String = format!("../yew/avatars/{}", user_id.clone().to_string());
    let destination_api: String = format!("./avatars/{}", user_id.clone().to_string());
    while let Ok(Some(mut field)) = payload.try_next().await {
       
        let mut saved_file: File = File::create(&destination).unwrap();
        let mut saved_file_api: File = File::create(&destination_api).unwrap();
        while let Ok(Some(chunk)) = field.try_next().await {
            let _ = saved_file.write_all(&chunk.clone()).unwrap();
            let _ = saved_file_api.write_all(&chunk).unwrap();
        }
    } 
    let avatar_path = format!("../avatars/{}", user_id.clone());
    db::interaction::change_user_avatar(avatar_path.clone(), user_id.clone());
    web::Redirect::to("http://localhost:3000/account").see_other()
}
#[get("/api/compositions/{ideology_id}")]
async fn compositions(path: web::Path<(i32,)>) -> impl Responder {
    web::Json(db::interaction::show_compositions(path.into_inner().0))
}
#[get("/api/favorite_compositions/{user_id}")]
async fn show_favorite_compositions(path: web::Path<(i32,)>) -> impl Responder {
    web::Json(db::interaction::show_favorite_compositions(path.into_inner().0))
}

#[get("/api/authors/{author_id}")]
async fn author(path: web::Path<(i32,)>) -> impl Responder {
    web::Json(db::interaction::show_author(path.into_inner().0))
}
#[get("/api/delete_user/{user_id}")]
async fn delete_user(path: web::Path<(i32,)>) -> impl Responder {
    db::interaction::delete_user(path.into_inner().0);
    String::from("Deleted")
}

#[post("/api/add_favorite")]
async fn add_favorite(form: web::Form<FavoriteForm>) -> impl Responder {
    db::interaction::add_favorite(form.user_id.clone(), form.composition_id.clone());
    String::from("Added favorite")
}

#[actix_web::main] // or #[tokio::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        let cors = Cors::default()
            .allowed_origin("http://localhost:3000")
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
            .allowed_header(http::header::CONTENT_TYPE)
            .max_age(3600);
        
        App::new()
        .wrap(cors)
        .service(show_favorite_compositions)
        .service(add_favorite)
        .service(show_dialog_friend)
        .service(create_friend_dialog)
        .service(upload_avatar)
        .service(show_dialog)
        .service(show_messages)
        .service(delete_user)
        .service(send_message)
        .service(author)
        .service(compositions)
        .service(add_friend)
        .service(friends)
        .service(users)
        .service(register)
        .service(login)
        .service(lesson)
    })
    .bind(("0.0.0.0", 8086))?
    .run()
    .await
}