// @generated automatically by Diesel CLI.

diesel::table! {
    authors (id) {
        id -> Int4,
        full_name -> Varchar,
        description -> Varchar,
        ideology_id -> Int4,
        image -> Varchar,
    }
}

diesel::table! {
    compositions (id) {
        id -> Int4,
        name -> Varchar,
        description -> Varchar,
        author -> Int4,
        image -> Varchar,
        type_id -> Int4,
        ideology -> Int4,
        path -> Varchar,
    }
}

diesel::table! {
    dialogmessage_files (id) {
        id -> Int4,
        file -> Varchar,
        dialogmessage_id -> Int4,
    }
}

diesel::table! {
    dialogmessage_photos (id) {
        id -> Int4,
        image -> Varchar,
        dialogmessage_id -> Int4,
    }
}

diesel::table! {
    dialogmessages (id) {
        id -> Int4,
        text -> Varchar,
        user_id -> Int4,
        dialog_id -> Int4,
    }
}

diesel::table! {
    dialogparticipants (id) {
        id -> Int4,
        user_id -> Int4,
        dialog_id -> Int4,
    }
}

diesel::table! {
    dialogs (id) {
        id -> Int4,
        name -> Varchar,
        image -> Varchar,
    }
}

diesel::table! {
    favoritecompositions (id) {
        id -> Int4,
        users_id -> Int4,
        compositions_id -> Int4,
    }
}

diesel::table! {
    friends (id) {
        id -> Int4,
        users_id -> Int4,
        friend_id -> Int4,
    }
}

diesel::table! {
    ideologies (id) {
        id -> Int4,
        name -> Varchar,
        image -> Varchar,
    }
}

diesel::table! {
    profiles (id) {
        id -> Int4,
        users_id -> Int4,
        full_name -> Nullable<Varchar>,
        email -> Varchar,
        avatar_path -> Nullable<Varchar>,
        ideology_id -> Int4,
        birthday -> Nullable<Date>,
    }
}

diesel::table! {
    roles (id) {
        id -> Int4,
        name -> Varchar,
    }
}

diesel::table! {
    typecompositions (id) {
        id -> Int4,
        name -> Varchar,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        password -> Varchar,
        roles_id -> Int4,
    }
}

diesel::joinable!(compositions -> authors (author));
diesel::joinable!(compositions -> typecompositions (type_id));
diesel::joinable!(dialogmessage_photos -> dialogmessages (dialogmessage_id));
diesel::joinable!(favoritecompositions -> compositions (compositions_id));
diesel::joinable!(friends -> users (friend_id));
diesel::joinable!(users -> roles (roles_id));

diesel::allow_tables_to_appear_in_same_query!(
    authors,
    compositions,
    dialogmessage_files,
    dialogmessage_photos,
    dialogmessages,
    dialogparticipants,
    dialogs,
    favoritecompositions,
    friends,
    ideologies,
    profiles,
    roles,
    typecompositions,
    users,
);
