use diesel::prelude::*;
use serde::Serialize;

#[derive(Queryable, Selectable, Serialize)]
#[diesel(belongs_to(Users))]
#[diesel(belongs_to(Users))]
#[diesel(table_name = crate::db::schema::friends)]
pub struct Friends {
    pub friend_id: i32,
    pub users_id: i32,
}

#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::users)]
pub struct Users {
    pub id: i32,
    pub username: String,
    pub password: String,
}

#[derive(Insertable)]
#[diesel(table_name = crate::db::schema::users)]
pub struct NewUser {
    pub username: String,
    pub password: String,
}



#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::dialogs)]
pub struct Dialogs {
    pub id: i32,
    pub name: String,
    pub image: String,
}

#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::profiles)]
pub struct Profiles {
    pub id: i32,
    pub users_id: i32,
    pub full_name: Option<String>,
    pub email: String,
    pub avatar_path: Option<String>,
    pub ideology_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = crate::db::schema::profiles)]
pub struct NewProfile {
    pub users_id: i32,
    pub email: String,
    pub ideology_id: i32,
}

#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::compositions)]
pub struct Compositions{
    pub id: i32,
    pub name: String,
    pub description: String,
    pub author: i32,
    pub image: String,
    pub type_id: i32, 
    pub ideology: i32,
}

#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::authors)]
pub struct Authors{
    pub full_name: String,
    pub description: String,
    pub ideology_id: i32,
    pub image: String,
}

#[derive(Identifiable, Selectable, Queryable, Associations)]
#[diesel(belongs_to(Compositions))]
#[diesel(belongs_to(Users))]
#[diesel(table_name = crate::db::schema::favoritecompositions)]
pub struct FavoriteCompositions{
    pub id: i32,
    pub users_id: i32,
    pub compositions_id: i32,
}

#[derive(Queryable, Selectable, Serialize)]
#[diesel(belongs_to(Users))]
#[diesel(belongs_to(Dialogs))]
#[diesel(table_name = crate::db::schema::dialogparticipants)]
pub struct DialogParticipants {
    pub user_id: i32,
    pub dialog_id: i32,
}
#[derive(Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::db::schema::dialogmessages)]
pub struct DialogMessages {
    pub text: String,
    pub user_id: i32,
    pub dialog_id: i32,
}
