use diesel::pg::PgConnection;
use diesel::prelude::*;
use crate::db::models::{
    Users, 
    NewUser, 
    NewProfile, 
    Profiles, 
    Friends,
    Compositions,
    Authors,
    Dialogs,
    DialogMessages,
};
use crate::db::schema::authors::dsl::*;
use crate::db::schema::compositions::dsl::*;
use crate::db::schema::favoritecompositions::dsl::*;
use crate::db::schema::friends::dsl::*;
use crate::db::schema::dialogparticipants::dsl::*;
use crate::db::schema::dialogmessages::dsl::*;
use crate::db::schema::dialogs::dsl::*;
use crate::db::schema::users::dsl::*;
use crate::db::schema::profiles::dsl::*;

#[derive(Clone, serde::Serialize)]
pub struct FullUserInfo {
    id: i32,
    username: String,
    email: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
    password: String,
}

#[derive(Clone, serde::Serialize)]
pub struct FullUserInfoChat {
    id: i32,
    username: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
    text: String,
}

#[derive(Clone, serde::Serialize)]
pub struct FullUserInfoWithDialog {
    id: i32,
    username: String,
    ideology: i32,
    full_name: Option<String>,
    avatar_path: Option<String>,
    dialog_id: i32,
}

#[derive(Clone, serde::Serialize)]
pub struct Composition {
    id: i32,
    name: String,
    description: String,
    author: String,
    image: String,
    type_id: i32, 
    ideology: i32,
}
pub fn establish_connection() -> PgConnection {

    let database_url = String::from("postgres://admin:root@postgres:5432/postgres?sslmode=disable");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn add_friend(id_user: i32, id_friend: i32){
    let connection = &mut establish_connection();

    diesel::insert_into(friends)
        .values((
            crate::db::schema::friends::users_id.eq(id_user),
            crate::db::schema::friends::friend_id.eq(id_friend)
        ))
        .execute(connection)
        .expect("Error saving new friend");
}

pub fn create_friend_dialog(dialog_name: String, dialog_image: String, id_user: i32, id_friend: i32){
    let connection = &mut establish_connection();

    let new_dialog: i32 = diesel::insert_into(dialogs)
        .values((
            crate::db::schema::dialogs::name.eq(dialog_name),
            crate::db::schema::dialogs::image.eq(dialog_image)
        ))
        .returning(crate::db::schema::dialogs::id)
        .get_result(connection)
        .expect("Error saving new user");
    diesel::insert_into(dialogparticipants)
        .values((
            crate::db::schema::dialogparticipants::user_id.eq(id_user),
            crate::db::schema::dialogparticipants::dialog_id.eq(new_dialog.clone())
        ))
        .execute(connection)
        .expect("Error saving new user");
    diesel::insert_into(dialogparticipants)
        .values((
            crate::db::schema::dialogparticipants::user_id.eq(id_friend),
            crate::db::schema::dialogparticipants::dialog_id.eq(new_dialog)
        ))
        .execute(connection)
        .expect("Error saving new user");
    
}

pub fn show_dialog(id_user: i32) -> Vec<FullUserInfoWithDialog>{
    let connection = &mut establish_connection();

    let dialogs_id: Vec<i32> = dialogparticipants
        .filter(crate::db::schema::dialogparticipants::user_id.eq(id_user.clone()))
        .select(crate::db::schema::dialogparticipants::dialog_id)
        .load(connection)
        .expect("Error loading users");
    let mut result = Vec::<FullUserInfoWithDialog>::new();
    for dialog in &dialogs_id{
        let dialog_user: i32 = dialogparticipants
            .filter(crate::db::schema::dialogparticipants::dialog_id.eq(dialog.clone()))
            .filter(crate::db::schema::dialogparticipants::user_id.ne(id_user.clone()))
            .select(crate::db::schema::dialogparticipants::user_id)
            .first(connection)
            .expect("Error loading users");
        let user = users
            .filter(crate::db::schema::users::id.eq(dialog_user.clone()))
            .select(Users::as_select())
            .first::<Users>(connection)
            .expect("b");
        let profile = profiles
            .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
            .select(Profiles::as_select())
            .first::<Profiles>(connection)
            .expect("b");
        result.push(
            FullUserInfoWithDialog {
                id: user.id.clone(),
                username: user.username.clone(),
                ideology: profile.ideology_id.clone(),
                full_name: profile.full_name.clone(),
                avatar_path: profile.avatar_path.clone(),
                dialog_id: dialog.clone(),
            }
        );
    }   
    result
}

pub fn show_messages(id_dialog: i32) -> Vec<FullUserInfoChat>{
    let connection = &mut establish_connection();

    let messages: Vec<DialogMessages> = dialogmessages
        .filter(crate::db::schema::dialogmessages::dialog_id.eq(id_dialog.clone()))
        .select(DialogMessages::as_select())
        .load(connection)
        .expect("Error loading users");
    let mut result = Vec::<FullUserInfoChat>::new();
    for message in &messages{
        let user = users
            .filter(crate::db::schema::users::id.eq(message.user_id.clone()))
            .select(Users::as_select())
            .first::<Users>(connection)
            .expect("b");
        let profile = profiles
            .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
            .select(Profiles::as_select())
            .first::<Profiles>(connection)
            .expect("b");
        result.push(
            FullUserInfoChat {
                id: user.id.clone(),
                username: user.username.clone(),
                ideology: profile.ideology_id.clone(),
                full_name: profile.full_name.clone(),
                avatar_path: profile.avatar_path.clone(),
                text: message.text.clone(),
            }
        );
    }   
    result
   
}

pub fn show_dialog_friend(id_dialog: i32, id_user: i32) -> FullUserInfo{
    
    let connection = &mut establish_connection();
    let dialog: i32 = dialogparticipants
        .filter(crate::db::schema::dialogparticipants::dialog_id.eq(id_dialog.clone()))
        .filter(crate::db::schema::dialogparticipants::user_id.ne(id_user.clone()))
        .select(crate::db::schema::dialogparticipants::user_id)
        .first(connection)
        .expect("Error loading users");

    let user = users
        .filter(crate::db::schema::users::id.eq(dialog.clone()))
        .select(Users::as_select())
        .first::<Users>(connection)
        .expect("b");
    let profile = profiles
        .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
        .select(Profiles::as_select())
        .first::<Profiles>(connection)
        .expect("b");

        FullUserInfo{
            id: user.id.clone(),
            username: user.username.clone(),
            email: profile.email,
            ideology: profile.ideology_id,
            full_name: profile.full_name,
            avatar_path: profile.avatar_path,
            password: user.password,
        }
    
}

pub fn send_message(id_user: i32, id_dialog: i32, text_message: String){
    let connection = &mut establish_connection();

    diesel::insert_into(dialogmessages)
        .values((
            crate::db::schema::dialogmessages::user_id.eq(id_user),
            crate::db::schema::dialogmessages::dialog_id.eq(id_dialog),
            crate::db::schema::dialogmessages::text.eq(text_message),
        ))
        .execute(connection)
        .expect("Error saving new friend");
}

pub fn show_friends(id_user: i32) -> Vec<FullUserInfo>{
    let mut full_users_info = Vec::<FullUserInfo>::new();
    let connection = &mut establish_connection();
    let user_friends: Vec<i32> = friends
        .filter(crate::db::schema::friends::users_id.eq(id_user.clone()))
        .select(crate::db::schema::friends::friend_id)
        .load(connection)
        .expect("Error loading users");
    let friend_users: Vec<i32>  = friends
        .filter(crate::db::schema::friends::friend_id.eq(id_user.clone()))
        .select(crate::db::schema::friends::users_id)
        .load(connection)
        .expect("Error loading users");
    for user_friend_id in &user_friends{
        let user = users
            .filter(crate::db::schema::users::id.eq(user_friend_id.clone()))
            .select(Users::as_select())
            .first::<Users>(connection)
            .expect("b");
        let profile = profiles
            .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
            .select(Profiles::as_select())
            .first::<Profiles>(connection)
            .expect("b");
        full_users_info.push(
            FullUserInfo{
            id: user.id.clone(),
            username: user.username.clone(),
            email: profile.email,
            ideology: profile.ideology_id,
            full_name: profile.full_name,
            avatar_path: profile.avatar_path,
            password: user.password,
        });
    }   
    for friend_user_id in &friend_users{
        let user = users
            .filter(crate::db::schema::users::id.eq(friend_user_id.clone()))
            .select(Users::as_select())
            .first::<Users>(connection)
            .expect("b");
        let profile = profiles
            .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
            .select(Profiles::as_select())
            .first::<Profiles>(connection)
            .expect("b");
        full_users_info.push(
            FullUserInfo{
            id: user.id.clone(),
            username: user.username.clone(),
            email: profile.email,
            ideology: profile.ideology_id,
            full_name: profile.full_name,
            avatar_path: profile.avatar_path,
            password: user.password,
        });
    }   
    full_users_info
}

pub fn show_users() -> Vec<FullUserInfo>{
    let connection = &mut establish_connection();
    let mut full_users_info = Vec::<FullUserInfo>::new();
    let binding =  users
        .select(Users::as_select())
        .load(connection)
        .expect("a");
    let mut users_struct = binding.iter();
    while let Some(user) = users_struct.next(){
        let profile = profiles
            .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
            .select(Profiles::as_select())
            .first::<Profiles>(connection)
            .expect("b");
        full_users_info.push(
            FullUserInfo{
            id: user.id,
            username: user.username.clone(),
            email: profile.email,
            ideology: profile.ideology_id,
            full_name: profile.full_name,
            avatar_path: profile.avatar_path,
            password: user.password.clone(),
        });
    }
    full_users_info
}

pub fn show_compositions(id_ideology: i32) -> Vec<Composition>{
    let mut result = Vec::<Composition>::new();
    let connection = &mut establish_connection();
    if id_ideology.clone() == 5{
        let composition = compositions
            .select(Compositions::as_select())
            .load(connection)
            .expect("b");
        for c in &composition{
            let author_name = authors
                .filter(crate::db::schema::authors::id.eq(c.author.clone()))
                .select(Authors::as_select())
                .first::<Authors>(connection)
                .expect("b");
            result.push(
                Composition{
                    id: c.id.clone(),
                    name: c.name.clone(),
                    description: c.description.clone(),
                    author: author_name.full_name.clone(),
                    image: c.image.clone(),
                    type_id: c.type_id.clone(), 
                    ideology: c.ideology.clone(),
            });
        }
        return result;
    }

    let composition = compositions
        .filter(crate::db::schema::compositions::ideology.eq(id_ideology.clone()))
        .select(Compositions::as_select())
        .load(connection)
        .expect("b");
    for c in &composition{
        let author_name = authors
            .filter(crate::db::schema::authors::id.eq(c.author.clone()))
            .select(Authors::as_select())
            .first::<Authors>(connection)
            .expect("b");
        result.push(
            Composition{
                id: c.id.clone(),
                name: c.name.clone(),
                description: c.description.clone(),
                author: author_name.full_name.clone(),
                image: c.image.clone(),
                type_id: c.type_id.clone(), 
                ideology: c.ideology.clone(),
        });
    }
    result
}

pub fn add_favorite(id_user: i32, id_composition: i32){
    let connection = &mut establish_connection();
    
    diesel::insert_into(favoritecompositions)
    .values((
        crate::db::schema::favoritecompositions::users_id.eq(id_user),
        crate::db::schema::favoritecompositions::compositions_id.eq(id_composition)
    ))
    .execute(connection)
    .expect("Error saving new favorite");
}

pub fn show_author(id_author: i32) -> Authors{
    let connection = &mut establish_connection();
    authors
        .filter(crate::db::schema::authors::id.eq(id_author.clone()))
        .select(Authors::as_select())
        .first::<Authors>(connection)
        .expect("b")
}
pub fn show_favorite_compositions(id_user: i32) -> Vec<Composition>{
    let connection = &mut establish_connection();
    let mut result = Vec::<Composition>::new();
    let user_favoritecompositions = favoritecompositions
        .filter(crate::db::schema::favoritecompositions::users_id.eq(id_user.clone()))
        .inner_join(crate::db::schema::compositions::table)
        .select(Compositions::as_select())
        .load(connection)
        .expect("Error show favorite conpositions");
    for c in &user_favoritecompositions{
        let author_name = authors
            .filter(crate::db::schema::authors::id.eq(c.author.clone()))
            .select(Authors::as_select())
            .first::<Authors>(connection)
            .expect("b");
        result.push(
            Composition{
                id: c.id.clone(),
                name: c.name.clone(),
                description: c.description.clone(),
                author: author_name.full_name.clone(),
                image: c.image.clone(),
                type_id: c.type_id.clone(), 
                ideology: c.ideology.clone(),
        });
    }
    result
}

pub fn create_user(user_name: String, user_password: String) -> Users{
    let connection = &mut establish_connection();
    let new_user = NewUser{username: user_name, password: user_password};
    diesel::insert_into(users)
        .values(&new_user)
        .returning(Users::as_returning())
        .get_result(connection)
        .expect("Error saving new user")
}


pub fn create_profile(id_user: i32, user_email: String, id_ideology: i32){
    let connection = &mut establish_connection();
    let new_profile = NewProfile{users_id: id_user, email: user_email, ideology_id: id_ideology};
    diesel::insert_into(profiles)
        .values(&new_profile)
        .execute(connection)
        .expect("Error saving new profile");
}

pub fn check_user(user_name: String, user_password: String) -> Option<FullUserInfo>{
    let connection = &mut establish_connection();
    let user = users
        .filter(username.eq(user_name))
        .filter(password.eq(user_password))
        .select(Users::as_select())
        .first::<Users>(connection)
        .expect("a");
    let profile = profiles
        .filter(crate::db::schema::profiles::users_id.eq(user.id.clone()))
        .select(Profiles::as_select())
        .first::<Profiles>(connection)
        .expect("b");
    Some(FullUserInfo{
        id: user.id,
        username: user.username,
        email: profile.email,
        ideology: profile.ideology_id,
        full_name: profile.full_name,
        avatar_path: profile.avatar_path,
        password: user.password,
    })
}

pub fn change_user_avatar(path_avatar: String, id_user: i32){
    let connection = &mut establish_connection();
    diesel::update(profiles)
        .filter(crate::db::schema::profiles::users_id.eq(id_user.clone()))
        .set(avatar_path.eq(path_avatar.clone()))
        .execute(connection);
}

pub fn delete_user(id_user: i32){
    let connection = &mut establish_connection();
    diesel::delete(users.filter(crate::db::schema::users::id.eq(id_user)))
        .execute(connection)
        .expect("Error deleting user");
}