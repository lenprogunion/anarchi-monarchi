CREATE TABLE IF NOT EXISTS dialogParticipants (
  id SERIAL PRIMARY KEY,
  user_id int NOT NULL,
  dialog_id int NOT NULL,
  CONSTRAINT fk_user
      FOREIGN KEY(user_id) 
	      REFERENCES users(id) ON DELETE CASCADE,
  CONSTRAINT fk_dialog
      FOREIGN KEY(dialog_id) 
	      REFERENCES dialogs(id) ON DELETE CASCADE
);