CREATE TABLE IF NOT EXISTS favoriteCompositions (
  id SERIAL PRIMARY KEY,
  users_id int NOT NULL,
  compositions_id int NOT NULL,
  CONSTRAINT fk_user
      FOREIGN KEY(users_id) 
	      REFERENCES users(id) ON DELETE CASCADE,
  CONSTRAINT fk_composition
      FOREIGN KEY(compositions_id) 
	      REFERENCES compositions(id) ON DELETE CASCADE
)