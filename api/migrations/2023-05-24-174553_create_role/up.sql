CREATE TABLE IF NOT EXISTS roles (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL
);

INSERT INTO roles (id, name) VALUES 
(1, 'Пользователь'),
(2, 'Администратор');
