CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username VARCHAR NOT NULL,
  password VARCHAR NOT NULL,
  roles_id int DEFAULT 1 NOT NULL,
  CONSTRAINT fk_role
      FOREIGN KEY(roles_id) 
	      REFERENCES roles(id)
)