CREATE TABLE IF NOT EXISTS compositions (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  description VARCHAR NOT NULL,
  author int NOT NULL,
  image VARCHAR NOT NULL,
  type_id int NOT NULL,
  ideology int NOT NULL,
  path VARCHAR NOT NULL,
  CONSTRAINT fk_author
      FOREIGN KEY(author) 
	      REFERENCES authors(id),
  CONSTRAINT fk_type
      FOREIGN KEY(type_id) 
	      REFERENCES typeCompositions(id),
  CONSTRAINT fk_ideology
      FOREIGN KEY(ideology) 
	      REFERENCES ideologies(id)
);

INSERT INTO compositions (id, name, description, author, image, type_id, ideology, path) VALUES 
(1, 'Капитал', 'главный труд немецкого философа и экономиста Карла Маркса по политической экономии, содержащий критический анализ капитализма. Работа написана с применением диалектико-материалистического подхода, в том числе к историческим процессам.', 1, 'https://cv2.litres.ru/pub/c/pdf-kniga/cover_max1500/26335624-author-karl_marks-kapital_kritika_politicheskoyi_yekonomii_tom_i.jpg', 1, 2, 'das'),
(2, 'Исследование о природе и причинах богатства народов', 'основная работа шотландского экономиста Адама Смита, опубликованная 9 марта 1776 года, во времена Шотландского просвещения. Книга оказала значительное влияние на экономическую теорию. Является основополагающим трудом классической политэкономии. ', 2, 'https://cdn.book24.ru/v2/ASE000000000845807/COVER/cover13d__w820.jpg', 1, 4, 'asd'),
(3, 'Взаимопомощь как фактор эволюции', 'Труд известного теоретика и организатора анархизма Петра Алексеевича Кропоткина. После 1917 года печатался лишь фрагментарно в нескольких сборниках, в частности, в книге "Анархия".', 3, 'https://img3.labirint.ru/rc/eb1a805d68b9f91e86357e2970b6eaf3/363x561q80/books40/393005/cover.jpg?1605111903', 1, 3, 'dasd'),
(4, 'Экономические софизмы', 'Французский экономист середины XIX века. Фредерик Бастиа в своих произведениях дал непревзойденную критику протекционистских заблуждений и намеренных передергиваний. Иронические, а иногда и сатирические, памфлеты Бастиа вызывали бессильный гнев сторонников "защиты отечественной промышленности и национального труда". Автор демонстрирует всю абсурдность протекционистской логики с точки зрения простого здравого смысла.', 4, 'https://cv3.litres.ru/pub/c/elektronnaya-kniga/cover/3949335-frederik-bastia-ekonomicheskie-sofizmy.jpg_330.jpg', 1, 1, 'asdsa');

