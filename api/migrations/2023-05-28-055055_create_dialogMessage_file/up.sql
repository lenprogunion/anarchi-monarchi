CREATE TABLE IF NOT EXISTS dialogMessage_files (
  id SERIAL PRIMARY KEY,
  file VARCHAR NOT NULL,
  dialogMessage_id int NOT NULL,
  CONSTRAINT fk_dialog
      FOREIGN KEY(dialogMessage_id) 
	      REFERENCES dialogMessages(id) ON DELETE CASCADE
)