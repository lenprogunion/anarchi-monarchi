CREATE TABLE IF NOT EXISTS friends (
  id SERIAL PRIMARY KEY,
  users_id int NOT NULL,
  friend_id int NOT NULL,
  CONSTRAINT fk_user
      FOREIGN KEY(users_id) 
	      REFERENCES users(id) ON DELETE CASCADE,
  CONSTRAINT fk_friend
      FOREIGN KEY(friend_id) 
	      REFERENCES users(id) ON DELETE CASCADE
)