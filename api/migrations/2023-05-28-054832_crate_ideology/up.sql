CREATE TABLE IF NOT EXISTS ideologies (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  image VARCHAR NOT NULL
)