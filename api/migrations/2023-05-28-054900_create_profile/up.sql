CREATE TABLE IF NOT EXISTS profiles (
  id SERIAL PRIMARY KEY,
  users_id int NOT NULL,
  full_name VARCHAR,
  email VARCHAR NOT NULL,
  avatar_path VARCHAR,
  ideology_id int NOT NULL,
  birthday DATE,
  CONSTRAINT fk_user
      FOREIGN KEY(users_id) 
	      REFERENCES users(id) ON DELETE CASCADE,
  CONSTRAINT fk_ideology
      FOREIGN KEY(ideology_id) 
	      REFERENCES ideologies(id)
)