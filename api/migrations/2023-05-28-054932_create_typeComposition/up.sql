CREATE TABLE IF NOT EXISTS typeCompositions (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL
);
INSERT INTO typeCompositions (id, name) VALUES 
(1, 'Книги'),
(2, 'Статьи');