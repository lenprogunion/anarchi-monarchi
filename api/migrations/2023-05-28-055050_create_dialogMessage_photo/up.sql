CREATE TABLE IF NOT EXISTS dialogMessage_photos (
  id SERIAL PRIMARY KEY,
  image VARCHAR NOT NULL,
  dialogMessage_id int NOT NULL,
  CONSTRAINT fk_dialogMessage
      FOREIGN KEY(dialogMessage_id) 
	      REFERENCES dialogMessages(id) ON DELETE CASCADE
)